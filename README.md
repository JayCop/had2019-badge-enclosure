Hackaday IO link

https://hackaday.io/project/168553-2019-superconference-badge-enclosure

Here's my Enclosure for the 2019 supercon badge. Not currently usable without modification. Features modeled PCB and Fusion source code. It has cutouts for nearly all ports, except those listed below.
Parameters that can be user adjusted:
Individual Button sizes
Button Fillets (roundness)
Port to cable tolerance
Case thickness
TODO:
Power switch
Split case
Cut out SAO connectors.
Allow cutouts to be optional via parameters. Is this possible? A quick search tells me it *might* be but results are mixed.
Cartridge.Light Pipes/LEDs.

Also see https://hackaday.io/project/168478-hackaday-superconference-2019-badge-enclosure by Tim Trzepacz who I spoke with a few times over the weekend about his badge case regarding things like attaching PCB to case, attaching the enclosure halves, and power switch/button operation.  Big thanks to Tim!